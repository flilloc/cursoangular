import { Component, EventEmitter, Output, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn  } from '@angular/forms'
import { DestinoViaje } from './../models/destino-viaje.model';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import {ReactiveFormsModule,FormsModule} from '@angular/forms';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  searchResults: string[];
  constructor(fb: FormBuilder) {
    console.log('llego');
    this.onItemAdded = new EventEmitter();
    console.log('entro');
    this.fg = fb.group({
      nombre: ['', Validators.compose([
              Validators.required,
              this.nombreValidator,
              this.nombreValidatorParametrizable(this.minLongitud)
            ])],
      url: ['']
    });
    this.fg.valueChanges.subscribe(
      (form: any) => {
        console.log('form cambió:', form);
      }
    );
   }

  ngOnInit() {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax('/assets/datos.json'))
    ).subscribe(ajaxResponse => {
      this.searchResults = ajaxResponse.response;
    });
  }
  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
   
      return {invalidNombre: true};
    }
      return null;
    }

    nombreValidatorParametrizable(minLong: number): ValidatorFn {
      return (control: FormControl): { [key: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
        if (l > 0 && l < minLong) {
              return { 'minLongNombre': true };
          }
          return null;
      };
  }
}
 