import { Component, OnInit, Input, HostBinding, EventEmitter, Output} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { VoteUpAction, VoteDownAction } from '../models/destinos-viajes-state.model';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  @Input() destino: DestinoViaje;
  @Input() position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<DestinoViaje>;

  constructor(
    private destinosApiClient:DestinosApiClient,private store: Store<AppState>
  ) {  
    this.onItemAdded = new EventEmitter();
    this.onClicked = new EventEmitter();
  }
  ngOnInit() {
  }
  ir(){
    console.log('entro');
    this.onClicked.emit(this.destino);
    return false;
  }
  agregado(d:DestinoViaje) {
    console.log('llego');
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
