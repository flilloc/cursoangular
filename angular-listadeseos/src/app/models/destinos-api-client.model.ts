import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import {AppState} from './../app.module';
import {
    NuevoDestinoAction,
    ElegidoFavoritoAction
  } from './destinos-viajes-state.model';
  
export class DestinosApiClient {
	destinos: DestinoViaje[];
	current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);
	constructor( private store: Store<AppState>) {
		this.destinos = [];
	}
	add(d: DestinoViaje) {
		this.destinos.push(d);
	}
	getAll() {
		return this.destinos;
	}
	 
	elegir(d: DestinoViaje) { 
		this.store.dispatch(new ElegidoFavoritoAction(d));
	  }

	  suscribeOnChange(fn){
		  this.current.subscribe(fn);
	  }
}